# Pink-footed goose migration

Analysis for the paper: "Rapid formation of new migration route and breeding area by Arctic geese" by Jesper Madsen, Kees H.T. Schreven, Gitte H. Jensen, Fred A. Johnson, Leif Nilsson, Bart A. Nolet and Jorma Pessa

DOI: 10.5281/zenodo.7501015

